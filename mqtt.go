/*
	shelly-recalibrate – correct the valve behaviour of Shelly TRVs

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"regexp"
	"time"

	tbr_logging "gitlab.com/bronger/tools/logging"

	"github.com/eclipse/paho.golang/paho"
)

var shellies []*shelly

// initShellies reads the configuration into the “shellies” data structure.
// Note that this is not an “init” function because it depends on “config”
// already initialised.
func initShellies() {
	for id := range config.Shellies {
		shellies = append(shellies, &shelly{id: id})
	}
}

var topicRegexp = regexp.MustCompile(`^shellies/(.*)/info$`)

// getHandler returns the MQTT handler for all topics we are listening to.  The
// handler first finds out which Shelly sent the info message.  Then, it
// analyses the log of that Shelly and triggers a calibration if necessary.
// All of that happens in a goroutine because the handler itself blocks the
// MQTT router.  In other words, no two handlers run concurrently.
func getHandler(ctx context.Context, logger tbr_logging.Logger, errs chan<- error) func(m *paho.Publish) {
	return func(m *paho.Publish) {
		go func() {
			var info struct {
				WifiStatus struct {
					Ip string `json:"ip"`
				} `json:"wifi_sta"`
				Thermostats []struct {
					Pos float64
				}
			}
			if err := json.Unmarshal(m.Payload, &info); err != nil {
				errs <- fmt.Errorf("Failed to parse JSON from MQTT message (%w): %s", err, m.Payload)
				return
			}
			var id string
			if res := topicRegexp.FindStringSubmatch(m.Topic); res == nil {
				// Panic because mistake on our side is more
				// probable than a broken MQTT server.
				panic(fmt.Errorf("Invalid topic: %v", m.Topic))
			} else {
				id = res[1]
			}
			var shelly *shelly
			for shellyIndex := range shellies {
				currentId := func() string {
					shelly = shellies[shellyIndex]
					shelly.Lock()
					defer shelly.Unlock()
					return shelly.id
				}()
				if currentId == id {
					goto FOUND
				}
			}
			// Panic because mistake on our side is more
			// probable than a broken MQTT server.
			panic(fmt.Errorf("Update of Shelly “%s”, which I did not subscribe to", id))
		FOUND:
			shelly.Lock()
			defer shelly.Unlock()
			shelly.ip = info.WifiStatus.Ip
			if shelly.ip == "" {
				errs <- fmt.Errorf("Invalid IP “%s” for Shelly “%s”.  MQTT message was: %s",
					shelly.ip, shelly.id, m.Payload)
			}
			shelly.pos = int(math.Round(info.Thermostats[0].Pos))
			logger.Debug("Received valve position", "id", id, "position", shelly.pos)
			sm, err := shelly.suspiciousMoves(ctx, logger)
			if err != nil {
				errs <- err
				return
			}
			if sm {
				if err := shelly.calibrate(ctx, logger); err != nil {
					errs <- err
					return
				}
				// Wait for the calibration to finish.  Note
				// that the lock on the Shelly is hold during
				// this time so that no other operations can
				// interfere.
				time.Sleep(10 * time.Second)
			} else {
				logger.Debug("No suspicious Shelly motor movement detected", "id", id)
			}
		}()
	}
}
