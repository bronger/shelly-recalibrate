module gitlab.com/bronger/shelly-recalibrate

go 1.22.0

require (
	github.com/eclipse/paho.golang v0.20.0
	gitlab.com/bronger/mqtt v0.0.0-20240222153632-7661c1bebd16
	gitlab.com/bronger/tools v0.0.0-20230825105701-52687403a66d
	go4.org v0.0.0-20230225012048-214862532bf5
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/net v0.17.0 // indirect
)
