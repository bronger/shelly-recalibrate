/*
	shelly-recalibrate – correct the valve behaviour of Shelly TRVs

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/bronger/mqtt"
	tbr_logging "gitlab.com/bronger/tools/logging"
)

// init initialises logging on the DEBUG level.
func init() {
	tbr_logging.Init(os.Stderr, slog.LevelDebug)
}

var name string

func init() {
	name = os.Getenv("SHELLY_RECALIBRATE_NAME")
	if name == "" {
		panic("You need to set the environment variable SHELLY_RECALIBRATE_NAME")
	}
}

// mqttReady is closed once mqtt.DefaultClient has got a valid value.
var mqttReady = make(chan struct{})

func main() {
	initShellies()
	baseCtx, baseCtxStop := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	errs := make(chan error)
	var errsWG sync.WaitGroup
	errsWG.Add(1)
	go func() {
		defer errsWG.Done()
		for err := range errs {
			if errors.Is(err, context.Canceled) {
				slog.Debug("Asynchronous routine reported “context cancelled”.  Ignoring")
			} else {
				slog.Warn("Fatal error in asynchronous routine", "error", err)
				baseCtxStop()
			}
		}
	}()
	topics := make(mqtt.Topics)
	handler := getHandler(baseCtx, slog.With("component", "monitor"), errs)
	for id := range config.Shellies {
		topic := fmt.Sprintf("shellies/%s/info", id)
		topics.Add(topic, handler)
	}
	mqtt.DefaultClient = mqtt.GetMQTTClient(baseCtx, slog.With("component", "MQTT"), config.MqttBroker,
		"shelly-recalibrate-"+name, topics)
	close(mqttReady)
	ticker := time.Tick(time.Hour)
LOOP:
	for {
		select {
		case <-ticker:
			for _, s := range shellies {
				if err := s.keepAlive(baseCtx, slog.With("component", "shellies")); err != nil {
					slog.Error("Error when trying to keep Shelly alive", "shelly", s.id, "error", err)
				} else {
					slog.Debug("Shelly is still alive", "shelly", s.id)
				}
			}
		case <-baseCtx.Done():
			break LOOP
		}
	}

	slog.Info("Shutting down …")
	baseCtxStop()
	mqtt.WaitForShutdown()
	close(errs)
	errsWG.Wait()
	slog.Info("Shutdown completed")
}
