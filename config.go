/*
	shelly-recalibrate – correct the valve behaviour of Shelly TRVs

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"os"

	tbr_errors "gitlab.com/bronger/tools/errors"

	"gopkg.in/yaml.v2"
)

type configType struct {
	MqttBroker string         `yaml:"mqttBroker"`
	Shellies   map[string]any `yaml:"heaters"`
}

var config configType

// init reads the configuration file and poplates the global “config” variable.
func init() {
	if len(os.Args) != 2 {
		tbr_errors.ExitWithExpectedError("invalid number of arguments", 10)
	}
	path := os.Args[1]
	yamlRaw, err := os.ReadFile(path)
	tbr_errors.ExitOnExpectedError(err, "configuration could not be read", 10, "path", path)
	err = yaml.Unmarshal(yamlRaw, &config)
	tbr_errors.ExitOnExpectedError(err, "configuration could not be parsed", 10, "path", path)
	if config.MqttBroker == "" {
		tbr_errors.ExitWithExpectedError("configuration invalid: mqttServer empty or not defined", 10, "path", path)
	}
	if len(config.Shellies) == 0 {
		tbr_errors.ExitWithExpectedError("configuration invalid: no Shelly defined", 10, "path", path)
	}
}
