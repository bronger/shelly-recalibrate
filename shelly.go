/*
	shelly-recalibrate – correct the valve behaviour of Shelly TRVs

Copyright (C) 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/bronger/mqtt"
	tbr_logging "gitlab.com/bronger/tools/logging"
	"go4.org/must"
)

type shelly struct {
	sync.Mutex
	id, ip string
	pos    int
}

// getLog returns the current log of a Shelly.
func (s *shelly) getLog(ctx context.Context, logger tbr_logging.Logger) (string, error) {
	body, err := s.httpGET(ctx, logger, "debug/log", 10)
	if err != nil {
		logger.Warn("Could not get log.  Reboot and retry once …", "id", s.id, "error", err)
		if err := s.reboot(ctx, logger); err != nil {
			logger.Warn("Error after reboot ignored", "id", s.id, "error", err)
		}
		time.Sleep(30 * time.Second)
		if body, err = s.httpGET(ctx, logger, "debug/log", 10); err != nil {
			return "", err
		}
		logger.Info("Could get log after reboot", "id", s.id)
		<-mqttReady
		if err := mqtt.PublishMsg(ctx, logger,
			fmt.Sprintf("shellies/%s/thermostat/0/command/valve_pos", s.id), s.pos); err != nil {
			return "", err
		}
	}
	var occurences int
	cleanedBody := make([]byte, 0, len(body))
	for _, c := range body {
		if c == 0xf3 {
			occurences++
		} else {
			cleanedBody = append(cleanedBody, c)
		}
	}
	if occurences > 0 {
		logger.Warn("Invalid characters in the Shelly log", "number", occurences, "shelly", s.id)
	}
	return string(cleanedBody), nil
}

var (
	stepsRegexp = regexp.MustCompile(`Steps: requested: (\d+) moved: (\d+)\s`)
	valveRegexp = regexp.MustCompile(`(\d+(?:\.\d*)?)\s*stepper_set_percent:\s*` +
		`Stepper set to (\d+(?:\.\d*)?)%\((\d+(?:\.\d*)?)%\) \((\d+)\)`)
	unixtimeRegexp = regexp.MustCompile(`(?m)^(\d{10,}(\.\d*)?)\s`)
	valvePosRegexp = regexp.MustCompile(`(\d+(?:\.\d*)?)%\(`)
)

type timestampLoc struct {
	timestamp time.Time
	index     int
}

// collectTimestamps returns the value and location of all timestamps in the
// given log.  This can be used to tag an arbitrary part of the log with a
// timestamp.  This is necessary because the Shelly log is chaotic: It does not
// consist of clear lines which always start with a timestamp.
func collectTimestamps(log string) (timestampLocs []timestampLoc) {
	for _, indices := range unixtimeRegexp.FindAllStringSubmatchIndex(log, -1) {
		unixtimeRaw := log[indices[2]:indices[3]]
		if unixtime, err := strconv.ParseFloat(unixtimeRaw, 64); err != nil {
			panic("Regular expression for a float didn’t yield a valid float")
		} else {
			timestampLocs = append(timestampLocs, timestampLoc{
				time.UnixMilli(int64(unixtime * 1000)),
				indices[3]})
		}
	}
	return
}

// findTimestamp returns the timestamp associated with the given character
// position in the log.  The “timestampLocs” parameter is created by
// “collectTimestamps”.
func findTimestamp(timestampLocs []timestampLoc, pos int) time.Time {
	for i := range timestampLocs {
		loc := timestampLocs[len(timestampLocs)-1-i]
		if loc.index < pos {
			return loc.timestamp
		}
	}
	return time.Time{}
}

type valveLoc struct {
	valve float64
	index int
}

// collectValves does the sample thing as “collectTimestamps” but for valve
// positions instead of timestamps.
func collectValves(log string) (valveLocs []valveLoc) {
	for _, indices := range valvePosRegexp.FindAllStringSubmatchIndex(log, -1) {
		valveRaw := log[indices[2]:indices[3]]
		if valve, err := strconv.ParseFloat(valveRaw, 64); err != nil {
			panic("Regular expression for a float didn’t yield a valid float")
		} else {
			valveLocs = append(valveLocs, valveLoc{valve, indices[1]})
		}
	}
	return
}

// findValve does the sample thing as “findTimestamp” but for valve positions
// instead of timestamps.
func findValve(valveLocs []valveLoc, pos int) float64 {
	for _, loc := range valveLocs {
		if loc.index > pos {
			return loc.valve
		}
	}
	return math.NaN()
}

const (
	calibEndMarker    = "Calibration passed"
	calibFailedMarker = "Calibration failed"
)

// suspiciousMoves returns whether the current log of the Shelly indicates that
// a calibration is recommended/necessary.  This is true if there are motor
// movement lines with “requested” ≠ “moved”, or if the valve was fully closed.
// Of couse, no calibration must occur in the log *after* such suspicious
// lines.
func (s *shelly) suspiciousMoves(ctx context.Context, logger tbr_logging.Logger) (bool, error) {
	log, err := s.getLog(ctx, logger)
	if err != nil {
		return false, err
	}
	rest := log
	var afterCalib bool
	if index := strings.LastIndex(rest, calibEndMarker); index != -1 {
		rest = rest[index+len(calibEndMarker):]
		afterCalib = true
	}
	if index := strings.Index(rest, calibFailedMarker); index != -1 {
		logger.Debug("Failed calibration detected", "id", s.id)
		return true, nil
	}
	timestampLocs := collectTimestamps(rest)
	valveLocs := collectValves(rest)
	for i, indices := range stepsRegexp.FindAllStringSubmatchIndex(rest, -1) {
		if !afterCalib || i != 0 {
			var requested, moved int
			if requested, err = strconv.Atoi(rest[indices[2]:indices[3]]); err != nil {
				panic("Regular expression for an int didn’t yield a valid int")
			}
			if moved, err = strconv.Atoi(rest[indices[4]:indices[5]]); err != nil {
				panic("Regular expression for an int didn’t yield a valid int")
			}
			if requested != moved {
				timestamp := findTimestamp(timestampLocs, indices[0])
				logger.Debug("Suspicious move detected", "id", s.id,
					"unixtime", timestamp.Unix(), "timestamp", timestamp,
					"valve", findValve(valveLocs, indices[1]),
					"requested", requested, "moved", moved)
				return true, nil
			}
		}
	}
	for i, indices := range valveRegexp.FindAllStringSubmatchIndex(rest, -1) {
		if !afterCalib || i != 0 {
			valveRaw := rest[indices[4]:indices[5]]
			var valve float64
			if valve, err = strconv.ParseFloat(valveRaw, 64); err != nil {
				panic("Regular expression for a float didn’t yield a valid float")
			}
			if valve == 0 {
				timestamp := findTimestamp(timestampLocs, indices[0])
				logger.Debug("Full closing of valve detected", "id", s.id,
					"unixtime", timestamp.Unix(), "timestamp", timestamp)
				return true, nil
			}
		}
	}
	return false, nil
}

// calibrate initiates a calibration of the Shelly.  Note that this does not
// wait for the calibration to finish.
func (s *shelly) calibrate(ctx context.Context, logger tbr_logging.Logger) error {
	if body, err := s.httpGET(ctx, logger, "calibrate", 10); err != nil {
		return err
	} else {
		var res struct {
			Ok bool `json:"ok"`
		}
		err := json.Unmarshal(body, &res)
		if err != nil {
			return fmt.Errorf("Could not parse response from Shelly: %w", err)
		} else if !res.Ok {
			return fmt.Errorf("Shelly did not report success after calibration: %s", body)
		}
	}
	logger.Info("Calibration triggered", "id", s.id)
	return nil
}

// reboot initiates a reboot of the Shelly.  Note that this does not wait for
// the reboot to finish.
func (s *shelly) reboot(ctx context.Context, logger tbr_logging.Logger) error {
	if body, err := s.httpGET(ctx, logger, "reboot", 0); err != nil {
		return err
	} else {
		var res struct {
			Ok bool `json:"ok"`
		}
		err := json.Unmarshal(body, &res)
		if err != nil {
			return fmt.Errorf("Could not parse response from Shelly: %w", err)
		} else if !res.Ok {
			return fmt.Errorf("Shelly did not report success after calibration: %s", body)
		}
	}
	logger.Info("Reboot triggered", "id", s.id)
	return nil
}

// keepAlive makes sure that the Shelly responds to HTTP requests.  If not, it
// tries whether a reboot helps, but only once.
func (s *shelly) keepAlive(ctx context.Context, logger tbr_logging.Logger) error {
	s.Lock()
	defer s.Unlock()
	_, err := s.getLog(ctx, logger)
	return err
}

// httpGET make a HTTP GET request against the shelly.  relURL is without the
// leading slash but it is appended directly after the domain name.
func (s *shelly) httpGET(ctx context.Context, logger tbr_logging.Logger, relURL string, retries int) (
	body []byte, err error) {
	url := "http://" + s.ip + "/" + relURL
	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, "GET", url, nil); err != nil {
		panic(err)
	} else {
		for {
			var resp *http.Response
			if resp, err = http.DefaultClient.Do(req); err != nil {
				err = fmt.Errorf("Could not get HTTP response from “%s”: %w", url, err)
			} else {
				defer must.Close(resp.Body)
				if body, err = io.ReadAll(resp.Body); err != nil {
					err = fmt.Errorf("Could not read HTTP response from “%s”: %w", url, err)
				} else {
					return
				}
			}
			if retries <= 0 {
				return
			}
			retries--
			logger.Warn("Retry", "error", err, "retries left", retries)
			time.Sleep(5 * time.Second)
		}
	}
}
