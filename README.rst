Shelly-Recalibrate
==================

The Shelly TRV sometimes loses its calibration – the motor does not move the
way it should.  Fortunately, this can be detected in the log.

This program can be used as a headless, long-running process.  It detects
changes in the valve position via MQTT, and if inconsistencies occur in the
log, a calibration is initiated.


How does it work, exactly?
--------------------------

1. It subscribes to the MQTT topic ``shellies/my-shelly/info``.
2. When a message arrives, it assumes that the valve was moved.
3. The log of the Shelly is read via HTTP.
4. If the log contains suspicious lines, the calibration is initiated via HTTP.

“Suspicious” are log lines of the following form::

  Steps: requested: 152 moved: 12

That is, the number of motor steps needed was not realised.  (Immediately after
a calibration they are okay, though.)


Does is solve the issue?
------------------------

I can only say that it helps in my case.  It will certainly drain the battery
faster than without it.  Furthermore, the detection of situations when
calibration is needed cannot be perfect.  For instance, there may be races with
other actors that manipulate the Shelly.  Moreover, the log of the Shelly is
poor, allowing for concurrent writes that lead to garbled output like::

  1688109464.Calibrating.104 stepper_.. set_percent:

And, I do not know how good the calibration process itself is.  I may leave the
Shelly in a still uncalibrated state.


Invocation
----------

Call the program like this::

  SHELLY_RECALIBRATE_NAME=living-room shelly-recalibrate my-config.yaml

Here, ``my-config.yaml`` is the path to the configuration file.  The program
will log to stderr.  It runs until terminated by a signal (Ctrl-C, SIGINT,
SIGTERM, etc).

The environment variable ``SHELLY_RECALIBRATE_NAME`` allows the program to
generate a unique client ID agains the MQTT broker.


Configuration file
------------------

The configuration file might look like this::

  mqttBroker: tcp://raspi:1883
  heaters:
    front-wall:
    back-wall:

``tcp://raspi:1883`` contains hostname and port of your MQTT broker.  If
shelly-recalibrate runs on the same system as the broker, you may pass
``tcp://localhost:1883``.

“front-wall” and “back-wall” are the “Custom MQTT prefixes” in the MQTT
configuration of the devices.


How to install
--------------

If you have installed Go in your system, you may install shelly-recalibrate
with::

  go install gitlab.com/bronger/shelly-recalibrate

The source code is located at <https://gitlab.com/bronger/shelly-recalibrate>.
Please use that location also to report issues.  If you wish to compile for
yourself, clone the repository to your system and say::

  go build

To compile for RaspberryPi, say::

  GOOS=linux GOARCH=arm GOARM=5 go build

Contact bronger@physik.rwth-aachen.de if you need binaries and can’t compile
them yourself.


Licence
-------

Copyright © 2023 Torsten Bronger, bronger@physik.rwth-aachen.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
